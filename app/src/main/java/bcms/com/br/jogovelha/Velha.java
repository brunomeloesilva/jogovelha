package bcms.com.br.jogovelha;

import android.content.Context;
import android.graphics.Canvas;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
/**
 * Created by brunosilva on 18/12/15.
 */
public class Velha extends View {
	
	private int larguraTela;
	private int alturaTela;
	// Definição
	private float tamanhoRaio;
	private float tamanhoLinha;
	// Linha Horizontal Superior = LHSx 
	private float coordenadaX_LHSx;
	private float coordenadaY_LHSy;
	// Linha Vertical Esquerda = LVEx 
	private float coordenadaX_LVEx;
	private float coordenadaY_LVEy;
	// Linha Vertical Direita = LVDx
	private float coordenadaX_LVDx;
	// Linha Horizontal Inferior = LVI
	private float coordenadaY_LVIy;
	// é X ou _O
	private boolean isX = true;
	//Quadrantes da velha, preenchidos com X=1 ou _O=0 quando usados, senao -1.
	private byte quadrante[] = {-1, -2, -3, -4, -5, -6, -7, -8, -9};//9 quadrantes
	

	public Velha(Context context) {
		super(context);
		// Pegar as dimensoes da tela
		Display tela = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		alturaTela = tela.getHeight();
		larguraTela = tela.getWidth();
		// Configura a view para receber foco e tratar eventos de teclado
		setFocusable(true);
	}

	private void desenhaVelha(Canvas canvas) {
		// Cor dos fundos da velha
		setBackgroundColor(Preferencias.CORFUNDO.getColor());
		// Definicoes:
		//_O tamanho do raio é 2.6% da largura da tela
		tamanhoRaio = (larguraTela * 2.6f)/100f;
		//_O tamanho das linhas da velha é largura da tela - 10% dela
		tamanhoLinha = larguraTela - (larguraTela * 0.1f);

		//Para encontrar no plano cartesiano o ponto inicial do desenho:
		// Linha Vertical Esquerda = LVEx
		//Divide a largura da tela em 3 partes para achar a posicao de X no plano cartesiano
		coordenadaX_LVEx = (larguraTela / 3f);
		//Divide a altura da tela em 4 partes para achar a posicao de Y no plano cartesiano
		coordenadaY_LVEy = (alturaTela / 4f);

		// Linha Vertical Direita = LVDx
		//Dando o desconto da posicao do X inicial, eu acho a mesma distancia da tela no X final
		//Nao e citado aqui a coordenada de Y porque uso do anterior acima (coordenadaY_LVEy).
		coordenadaX_LVDx = larguraTela - coordenadaX_LVEx;

		// Linha Horizontal Superior = LHSx
		// Começa a uma distancia de 5% da extremidade da tela
		coordenadaX_LHSx = (larguraTela * 0.05f);
		coordenadaY_LHSy = coordenadaY_LVEy + (tamanhoLinha / 3f);

		// Linha Horizontal Inferior = LVI
		//Nao e citado aqui a coordenada de X porque uso do anterior acima (coordenadaX_LHSx).
		coordenadaY_LVIy = coordenadaY_LHSy + (tamanhoLinha / 3f);
		
		for (int i = 0; i <= tamanhoLinha; i++) {//Desenho todas as linhas da velha num FOR só
			// Linha Vertical Esquerda
			canvas.drawCircle(coordenadaX_LVEx, coordenadaY_LVEy + i, tamanhoRaio, Preferencias.COR_LINHA_VERTICAL_ESQUERDA);
			// Linha Vertical direita
			canvas.drawCircle(coordenadaX_LVDx, coordenadaY_LVEy + i, tamanhoRaio, Preferencias.COR_LINHA_VERTICAL_DIREITA);
			// Linha Horizontal superior
			canvas.drawCircle(coordenadaX_LHSx + i, coordenadaY_LHSy, tamanhoRaio, Preferencias.COR_LINHA_HORIZONTAL_SUPERIOR);
			// Linha Horizontal inferior
			canvas.drawCircle(coordenadaX_LHSx + i, coordenadaY_LVIy, tamanhoRaio, Preferencias.COR_LINHA_HORIZONTAL_INFERIOR);
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		desenhaVelha(canvas);
		redesenhaJogadas(canvas);
		//o if da dentro do metodo!
		desenhaLinhaFimJogo(canvas);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		//SE Quadrante 1 ainda nao tem valor, ou seja, nao foi preenchido com X ou _O. ENTAO
		//Verifica se o click dado foi na area do quadrante 1, se sim faz a jogada no quadrante 1
		//E assim por diante:
		/*
	 	*  1 | 2 | 3
	 	* ---+---+---
	 	*  4 | 5 | 6
	 	* ---+---+---
	 	*  7 | 8 | 9
	 	*/
		if( (quadrante[0] < 0) &&
			(event.getY() > coordenadaY_LVEy) && (event.getY() < (coordenadaY_LHSy-tamanhoRaio)) &&//linha 1 
			(event.getX() > coordenadaX_LHSx) && (event.getX() < (coordenadaX_LVEx-tamanhoRaio))  //coluna 1
		  )
		{
			fazJogada(0);
		}else if( (quadrante[1] < 0) &&
				  (event.getY() > coordenadaY_LVEy) && (event.getY() < (coordenadaY_LHSy-tamanhoRaio)) &&//linha 1 
				  (event.getX() > (coordenadaX_LVEx+tamanhoRaio)) && (event.getX() < (coordenadaX_LVDx-tamanhoRaio))//coluna 2
				) 
		{
			fazJogada(1);
		}else if( (quadrante[2] < 0) &&
				  (event.getY() > coordenadaY_LVEy) && (event.getY() < (coordenadaY_LHSy-tamanhoRaio)) &&//linha 1 
				  (event.getX() > (coordenadaX_LVDx+tamanhoRaio)) && (event.getX() < (coordenadaX_LHSx+tamanhoLinha))//coluna 3
				)
		{
			fazJogada(2);
		}else if( (quadrante[3] < 0) &&
				  (event.getY() > (coordenadaY_LHSy+tamanhoRaio)) && (event.getY() < (coordenadaY_LVIy-tamanhoRaio)) &&//linha 2
				  (event.getX() > coordenadaX_LHSx) && (event.getX() < (coordenadaX_LVEx-tamanhoRaio))//coluna 1
			    )
		{
			fazJogada(3);
		}else if( (quadrante[4] < 0) &&
				  (event.getY() > (coordenadaY_LHSy+tamanhoRaio)) && (event.getY() < (coordenadaY_LVIy-tamanhoRaio)) &&//linha 2
				  (event.getX() > (coordenadaX_LVEx+tamanhoRaio)) && (event.getX() < (coordenadaX_LVDx-tamanhoRaio))//coluna 2
			    )
		{
			fazJogada(4);
		}else if( (quadrante[5] < 0) &&
				  (event.getY() > (coordenadaY_LHSy+tamanhoRaio)) && (event.getY() < (coordenadaY_LVIy-tamanhoRaio)) &&//linha 2
				  (event.getX() > (coordenadaX_LVDx+tamanhoRaio)) && (event.getX() < (coordenadaX_LHSx+tamanhoLinha))//coluna 3
			    )
		{
			fazJogada(5);
		}else if( (quadrante[6] < 0) &&
				  (event.getY() > (coordenadaY_LVIy+tamanhoRaio)) && (event.getY() < (coordenadaY_LVEy+tamanhoLinha)) &&//linha 3
				  (event.getX() > coordenadaX_LHSx) && (event.getX() < (coordenadaX_LVEx-tamanhoRaio))//coluna 1
			    )
		{
			fazJogada(6);
		}else if( (quadrante[7] < 0) &&
				  (event.getY() > (coordenadaY_LVIy+tamanhoRaio)) && (event.getY() < (coordenadaY_LVEy+tamanhoLinha)) &&//linha 3
				  (event.getX() > (coordenadaX_LVEx+tamanhoRaio)) && (event.getX() < (coordenadaX_LVDx-tamanhoRaio))//coluna 2
			    )
		{
			fazJogada(7);
		}else if( (quadrante[8] < 0) &&
				  (event.getY() > (coordenadaY_LVIy+tamanhoRaio)) && (event.getY() < (coordenadaY_LVEy+tamanhoLinha)) &&//linha 3
				  (event.getX() > (coordenadaX_LVDx+tamanhoRaio)) && (event.getX() < (coordenadaX_LHSx+tamanhoLinha))//coluna 3
			    )
		{
			fazJogada(8);
		}
		return super.onTouchEvent(event);
	}

	private void redesenhaJogadas(Canvas canvas) {
		//para achar o ponto central do quadrante clicado, para desenhar a bola
		float meioY;

		float meioCirculoX;
		float ajusteX = (coordenadaX_LHSx+(coordenadaX_LHSx/2));//mesmo pra todos: fixo
		float pontoInicialX;
		float pontoInicialY;
		if(quadrante[0] > -1){
			meioCirculoX = (coordenadaX_LHSx + (coordenadaX_LVEx - tamanhoRaio)) / 2;//meioX;
			if(quadrante[0] == 1){//Se X
				//_Ponto inicial Diagonal secundaria
				pontoInicialX = meioCirculoX + ajusteX;
				pontoInicialY = (coordenadaY_LVEy + (coordenadaY_LHSy - tamanhoRaio)) / 2 - ajusteX;//meioY-ajusteX;
				desenhaX(canvas, pontoInicialX, pontoInicialY);
			}else {//Senao _O
				// valor central do intervalo de Y
				meioY = (coordenadaY_LVEy + (coordenadaY_LHSy - tamanhoRaio)) / 2;
				desenhaBola(canvas, meioY, meioCirculoX);
			}
		}

		if(quadrante[1] > -1){
			meioCirculoX = ((coordenadaX_LVEx+tamanhoRaio) + (coordenadaX_LVDx-tamanhoRaio))/2;//meioX;
			if(quadrante[1] == 1){
				//_Ponto inicial Diagonal secundaria
				pontoInicialX = meioCirculoX+ajusteX;
				pontoInicialY = ((coordenadaY_LVEy + (coordenadaY_LHSy-tamanhoRaio))/2)-ajusteX;//meioY-ajusteX;
				desenhaX(canvas, pontoInicialX, pontoInicialY);
			}else { 
				// valor central do intervalo de Y
				meioY = (coordenadaY_LVEy + (coordenadaY_LHSy-tamanhoRaio))/2;
				desenhaBola(canvas, meioY, meioCirculoX);
			}
		}

		if(quadrante[2] > -1){
			meioCirculoX = ((coordenadaX_LVDx+tamanhoRaio) + (coordenadaX_LHSx+tamanhoLinha))/2;//meioX;
			if(quadrante[2] == 1){ 
				//_Ponto inicial Diagonal secundaria
				pontoInicialX = meioCirculoX+ajusteX;
				pontoInicialY = ((coordenadaY_LVEy + (coordenadaY_LHSy-tamanhoRaio))/2)-ajusteX;//meioY-ajusteX;
				desenhaX(canvas, pontoInicialX, pontoInicialY);
			}else { 
				// valor central do intervalo de Y
				meioY = (coordenadaY_LVEy + (coordenadaY_LHSy-tamanhoRaio))/2;
				desenhaBola(canvas, meioY, meioCirculoX);
			}
		}

		if(quadrante[3] > -1){
			meioCirculoX = (coordenadaX_LHSx + (coordenadaX_LVEx-tamanhoRaio))/2;//meioX;
			if(quadrante[3] == 1){ 
				//_Ponto inicial Diagonal secundaria
				pontoInicialX = meioCirculoX+ajusteX;
				pontoInicialY = (((coordenadaY_LHSy+tamanhoRaio) + (coordenadaY_LVIy-tamanhoRaio))/2)-ajusteX;//meioY-ajusteX;
				desenhaX(canvas, pontoInicialX, pontoInicialY);
			}else { 
				// valor central do intervalo de Y
				meioY = ((coordenadaY_LHSy+tamanhoRaio) + (coordenadaY_LVIy-tamanhoRaio))/2;
				desenhaBola(canvas, meioY, meioCirculoX);
			}
		}

		if(quadrante[4] > -1){
			meioCirculoX = ((coordenadaX_LVEx+tamanhoRaio) + (coordenadaX_LVDx-tamanhoRaio))/2;//meioX;
			if(quadrante[4] == 1){ 
				//_Ponto inicial Diagonal secundaria
				pontoInicialX = meioCirculoX+ajusteX;
				pontoInicialY = (((coordenadaY_LHSy+tamanhoRaio) + (coordenadaY_LVIy-tamanhoRaio))/2)-ajusteX;//meioY-ajusteX;
				desenhaX(canvas, pontoInicialX, pontoInicialY);
			}else { 
				// valor central do intervalo de Y
				meioY = ((coordenadaY_LHSy+tamanhoRaio) + (coordenadaY_LVIy-tamanhoRaio))/2;
				desenhaBola(canvas, meioY, meioCirculoX);
			}
		}

		if(quadrante[5] > -1){
			meioCirculoX = ((coordenadaX_LVDx+tamanhoRaio) + (coordenadaX_LHSx+tamanhoLinha))/2;//meioX;
			if(quadrante[5] == 1){ 
				//_Ponto inicial Diagonal secundaria
				pontoInicialX = meioCirculoX+ajusteX;
				pontoInicialY = (((coordenadaY_LHSy+tamanhoRaio) + (coordenadaY_LVIy-tamanhoRaio))/2)-ajusteX;//meioY-ajusteX;
				desenhaX(canvas, pontoInicialX, pontoInicialY);
			}else { 
				// valor central do intervalo de Y
				meioY = ((coordenadaY_LHSy+tamanhoRaio) + (coordenadaY_LVIy-tamanhoRaio))/2;
				desenhaBola(canvas, meioY, meioCirculoX);
			}
		}

		if(quadrante[6] > -1){
			meioCirculoX = (coordenadaX_LHSx + (coordenadaX_LVEx-tamanhoRaio))/2;//meioX;
			if(quadrante[6] == 1){ 
				//_Ponto inicial Diagonal secundaria
				pontoInicialX = meioCirculoX+ajusteX;
				pontoInicialY = (((coordenadaY_LVIy+tamanhoRaio) + (coordenadaY_LVEy+tamanhoLinha))/2)-ajusteX;//meioY-ajusteX;
				desenhaX(canvas, pontoInicialX, pontoInicialY);
			}else { 
				// valor central do intervalo de Y
				meioY = ((coordenadaY_LVIy+tamanhoRaio) + (coordenadaY_LVEy+tamanhoLinha))/2;
				desenhaBola(canvas, meioY, meioCirculoX);
			}
		}

		if(quadrante[7] > -1){
			meioCirculoX = ((coordenadaX_LVEx+tamanhoRaio) + (coordenadaX_LVDx-tamanhoRaio))/2;//meioX;
			if(quadrante[7] == 1){ 
				//_Ponto inicial Diagonal secundaria
				pontoInicialX = meioCirculoX+ajusteX;
				pontoInicialY = (((coordenadaY_LVIy+tamanhoRaio) + (coordenadaY_LVEy+tamanhoLinha))/2)-ajusteX;//meioY-ajusteX;
				desenhaX(canvas, pontoInicialX, pontoInicialY);
			}else { 
				// valor central do intervalo de Y
				meioY = ((coordenadaY_LVIy+tamanhoRaio) + (coordenadaY_LVEy+tamanhoLinha))/2;
				desenhaBola(canvas, meioY, meioCirculoX);
			}
		}

		if(quadrante[8] > -1){
			meioCirculoX = ((coordenadaX_LVDx+tamanhoRaio) + (coordenadaX_LHSx+tamanhoLinha))/2;//meioX;
			if(quadrante[8] == 1){ 
				//_Ponto inicial Diagonal secundaria
				pontoInicialX = meioCirculoX+ajusteX;
				pontoInicialY = (((coordenadaY_LVIy+tamanhoRaio) + (coordenadaY_LVEy+tamanhoLinha))/2)-ajusteX;//meioY-ajusteX;
				desenhaX(canvas, pontoInicialX, pontoInicialY);
			}else { 
				// valor central do intervalo de Y
				meioY = ((coordenadaY_LVIy+tamanhoRaio) + (coordenadaY_LVEy+tamanhoLinha))/2;
				desenhaBola(canvas, meioY, meioCirculoX);
			}
		}
	}

	private void desenhaBola(Canvas canvas, float meioY, float meioX){
		float externoR = (larguraTela * 10.42f)/100f;// 80: 10.42%
		float internoR = (larguraTela * 6.51f)/100f;// 50: 6.51%
		canvas.drawCircle(meioX, meioY, externoR, Preferencias.COR_O);// 80: 10.42%
		canvas.drawCircle(meioX, meioY, internoR, Preferencias.CORFUNDO);// 50: 6.51%
	}
	
	private void desenhaX(Canvas canvas, float pontoInicialX, float pontoInicialY) {
		//Diagonal secundaria
		float aux = pontoInicialY;
		float tamanhoX = ((coordenadaX_LVEx-tamanhoRaio) - coordenadaX_LHSx);
		int tamanhoDiagonal = (int) (tamanhoX - (tamanhoX*0.40));//diminui 40%
		for (int i = 0; i < tamanhoDiagonal; i++) {
			canvas.drawCircle(pontoInicialX, pontoInicialY, tamanhoRaio, Preferencias.COR_X);
			pontoInicialX = pontoInicialX-1;//*
			pontoInicialY = pontoInicialY+1;
		}
		//Diagonal principal
		pontoInicialY = aux;
		for (int i = 0; i < tamanhoDiagonal; i++) {
			canvas.drawCircle(pontoInicialX, pontoInicialY, tamanhoRaio, Preferencias.COR_X);
			pontoInicialX = pontoInicialX+1;//*
			pontoInicialY = pontoInicialY+1;
		}
	}

	//Comeca a contar os quadrantes do 0.
	private void fazJogada(int quadrante){
		if(jogoTerminou() == -1) {//Se o jogo nao terminou, faz a jogada
			this.quadrante[quadrante] = (byte) (isX ? 1 : 0);
			isX = !isX;
			//Invalida a view, assim o metodo onDraw será chamado.
			invalidate();
		}else{//senao finaliza o jogo
			this.setEnabled(false);
		}
	}

	/* Verifica se o jogo terminou, se sim, indica a posicao de termino e inabilita a velha, senao -1.
	 *  1 | 2 | 3 -> L1 = 1
	 * ---+---+---
	 *  4 | 5 | 6 -> L2 = 2
	 * ---+---+---
	 *  7 | 8 | 9 -> L3 = 3
	 *  |   |   |
	 *  V   V   V
	 *  C1  C2  C3
	 *  =4  =5  =6
	 *
	 *  DP = 7; DS = 8; VELHOU = 9
	 */
	private int jogoTerminou() {
		if((quadrante[0]==quadrante[1])&&(quadrante[1]==quadrante[2])) return 1;
		else if((quadrante[3]==quadrante[4])&&(quadrante[3]==quadrante[5])) return 2;
		else if((quadrante[6]==quadrante[7])&&(quadrante[7]==quadrante[8])) return 3;
		else if((quadrante[0]==quadrante[3])&&(quadrante[3]==quadrante[6])) return 4;
		else if((quadrante[1]==quadrante[4])&&(quadrante[4]==quadrante[7])) return 5;
		else if((quadrante[2]==quadrante[5])&&(quadrante[5]==quadrante[8])) return 6;
		else if((quadrante[0]==quadrante[4])&&(quadrante[4]==quadrante[8])) return 7;
		else if((quadrante[2]==quadrante[4])&&(quadrante[4]==quadrante[6])) return 8;
		else if(quadrante[0] >= 0 && quadrante[1] >= 0 && quadrante[2] >= 0 &&
				quadrante[3] >= 0 && quadrante[4] >= 0 && quadrante[5] >= 0 &&
				quadrante[6] >= 0 && quadrante[7] >= 0 && quadrante[8] >= 0) return 9;
		else return -1;
	}

	//desenha linha de fim de jogo
	private void desenhaLinhaFimJogo(Canvas canvas){
		int posicao = jogoTerminou();
		if(posicao != -1) {//se o jogo terminou !
			float posicaoInicial = 0f;
			switch (posicao) {
				case 1:// Mesmo que meioY do quadrante 1 da classe Bola
					posicaoInicial = (coordenadaY_LVEy + (coordenadaY_LHSy - tamanhoRaio)) / 2;
					break;
				case 2:// Mesmo que meioY do quadrante 4 da classe Bola
					posicaoInicial = ((coordenadaY_LHSy + tamanhoRaio) + (coordenadaY_LVIy - tamanhoRaio)) / 2;
					;
					break;
				case 3:// Mesmo que meioY do quadrante 7 da classe Bola
					posicaoInicial = ((coordenadaY_LVIy + tamanhoRaio) + (coordenadaY_LVEy + tamanhoLinha)) / 2;
					;
					break;
				case 4:// Mesmo que meioX do quadrante 1 da classe Bola
					posicaoInicial = (coordenadaX_LHSx + (coordenadaX_LVEx - tamanhoRaio)) / 2;
					break;
				case 5:// Mesmo que meioX do quadrante 2 da classe Bola
					posicaoInicial = ((coordenadaX_LVEx + tamanhoRaio) + (coordenadaX_LVDx - tamanhoRaio)) / 2;
					break;
				case 6:// Mesmo que meioX do quadrante 3 da classe Bola
					posicaoInicial = ((coordenadaX_LVDx + tamanhoRaio) + (coordenadaX_LHSx + tamanhoLinha)) / 2;
					break;
			}

			if (posicao >= 1 && posicao <= 3) {
				for (int i = 0; i <= tamanhoLinha; i++)
					canvas.drawCircle(coordenadaX_LHSx + i, posicaoInicial, tamanhoRaio, Preferencias.COR_LINHA_GAMEOVER);
			} else if (posicao >= 4 && posicao <= 6) {
				for (int i = 0; i <= tamanhoLinha; i++)
					canvas.drawCircle(posicaoInicial, coordenadaY_LVEy + i, tamanhoRaio, Preferencias.COR_LINHA_GAMEOVER);
			} else if (posicao == 7) diagonalPrincipal(canvas);
			else if (posicao == 8) diagonalSecundaria(canvas);
			else if (posicao == 9) {
				diagonalPrincipal(canvas);
				diagonalSecundaria(canvas);
			}
		}
	}

	private void diagonalPrincipal(Canvas canvas) {
		float pontoInicialX = coordenadaX_LHSx;
		float pontoInicialY = coordenadaY_LVEy;
		for (int i = 0; i < tamanhoLinha; i++) {
			canvas.drawCircle(pontoInicialX, pontoInicialY, tamanhoRaio, Preferencias.COR_LINHA_GAMEOVER);
			pontoInicialX = pontoInicialX + 1;
			pontoInicialY = pontoInicialY + 1;
		}
	}

	private void diagonalSecundaria(Canvas canvas) {
		float pontoInicialX = coordenadaX_LHSx + tamanhoLinha;
		float pontoInicialY = coordenadaY_LVEy;
		for (int i = 0; i < tamanhoLinha; i++) {
			canvas.drawCircle(pontoInicialX, pontoInicialY, tamanhoRaio, Preferencias.COR_LINHA_GAMEOVER);
			pontoInicialX = pontoInicialX - 1;
			pontoInicialY = pontoInicialY + 1;
		}
	}

	public void restart(){
		isX = true;
		//quadrante[] = {-1, -2, -3, -4, -5, -6, -7, -8, -9};//9 quadrantes
		for (int i=0; i<9; i++){
			quadrante[i] = (byte) -(i+1);
		}
		invalidate();
	}
}
//- Criei este componente com alguns recursos adcionais, mas poderia ser evoluido para ter efeito
//de montagem com delay, efeitos, etc. Para fazer um migué...
//- Tem muita View junta, que na hora do redesenho, acaba desenhando tudo denovo....separar.
